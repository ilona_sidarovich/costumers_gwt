package com.hellogwt.client;

import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.ui.Button;
import com.hellogwt.client.service.TypeService;
import com.hellogwt.client.service.TypeServiceAsync;

import com.hellogwt.shared.domain.Type;
import com.hellogwt.shared.domain.enums.CostumerTitle;
import com.hellogwt.shared.exception.ServiceException;
import com.hellogwt.client.service.CostumerService;
import com.hellogwt.client.service.CostumerServiceAsync;
import com.hellogwt.shared.domain.Costumer;


import java.util.List;

public class CostumerGWTWidget extends Composite {

//   private static final Logger LOG = Logger.getLogger(HelloGWTWidget.class);

    interface HelloGWTWidgetUiBinder extends UiBinder<Widget, CostumerGWTWidget> {
    }

    private static HelloGWTWidgetUiBinder uiBinder = GWT.create(HelloGWTWidgetUiBinder.class);

    private CostumerServiceAsync costumerServiceAsync = GWT.create(CostumerService.class);
    private TypeServiceAsync typeServiceAsync = GWT.create(TypeService.class);

    private AsyncCallback<Void> callback = new AsyncCallback<Void>() {
        @Override
        public void onFailure(Throwable caught) {
            Window.alert("ERROR: Cannot editCostumer hellogwt!");
        }

        @Override
        public void onSuccess(Void result) {
            refreshGreetingsTable();
        }
    };

    @UiField
    TextBox firstNameTextBox;
    @UiField
    TextBox lastNameTextBox;
    @UiField
    Button addButton;
    @UiField
    ListBox titleBox;
    @UiField
    FlexTable costumersFlexTable;
    @UiField
    ListBox typeBox;


    public CostumerGWTWidget() {
        initWidget(uiBinder.createAndBindUi(this));
        refreshGreetingsTable();
    }

    @UiHandler("addButton")
    void handleAddButtonClick(ClickEvent clickEvent) {
        if (!firstNameTextBox.getText().isEmpty() && !lastNameTextBox.getText().isEmpty()) {
            Costumer costumer = new Costumer();
            Type type = new Type();
            costumer.setCostumerTitle(CostumerTitle.valueOf(titleBox.getValue(titleBox.getSelectedIndex()).toUpperCase()));
            costumer.setFirstName(firstNameTextBox.getText());
            costumer.setLastName(lastNameTextBox.getText());
            type.setCostumerType(typeBox.getValue(typeBox.getSelectedIndex()));
            costumer.setType(type);
            try {
                typeServiceAsync.createType(type,callback);
                costumerServiceAsync.createCostumer(costumer, callback);
            } catch (ServiceException e) {
                //LOG.error(e);
            }
        } else {
            Window.alert("\"First name\" and \"Last name\" fields cannot be empty!");
        }
    }

    //!!!!!!!!!!!!
//    @UiHandler("goodbyeLink")
//    void onClickGoodbye(ClickEvent e) {
//        presenter.goTo(new GoodbyePlace(name));
//    }

//    @UiHandler("updateButton")
//    void handleUpdateButtonClick(ClickEvent clickEvent) {
//        if (!firstNameTextBox.getText().isEmpty() && !lastNameTextBox.getText().isEmpty()) {
//            costumerServiceAsync.updateGreeting(firstNameTextBox.getText(), lastNameTextBox.getText(), callback);
//        } else {
//            Window.alert("\"Author\" and \"Text\" fields cannot be empty!");
//        }
//    }

    private void refreshGreetingsTable() {
        try {
            costumerServiceAsync.viewAllCostumers(new AsyncCallback<List<Costumer>>() {
                @Override
                public void onFailure(Throwable throwable) {
                    Window.alert("ERROR: Cannot load costumers!");
                }

                @Override
                public void onSuccess(List<Costumer> costumers) {
                    fillCostumersTable(costumers);
                }
            });
        } catch (ServiceException e) {
            e.printStackTrace();
        }
    }

    private void fillCostumersTable(List<Costumer> costumers) {
        costumersFlexTable.removeAllRows();
        costumersFlexTable.setText(0, 0, "Title");
        costumersFlexTable.setText(0, 1, "First Name");
        costumersFlexTable.setText(0, 2, "Last Name");
        costumersFlexTable.setText(0, 3, "Type");
        costumersFlexTable.setText(0, 4, "Remove");
        costumersFlexTable.setText(0, 5, "Edit");


        for (Costumer costumer : costumers) {
            final Costumer costumer1 = costumer;
            Button removeCostumerButton = new Button("x");
            final Long id =costumer1.getId();
            removeCostumerButton.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                    try {
                        costumerServiceAsync.deleteCostumer(id, callback);
                    } catch (ServiceException e) {
                        e.printStackTrace();
                    }
                }
            });
            Button editCostumerButton = new Button("Edit");
            editCostumerButton.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event) {
                        costumer1.setFirstName("Edited");
                }
            });
            EditTextCell editTextCell = new EditTextCell();
            Hyperlink link = new Hyperlink("Edit", "EditPage.html");
            int row = costumersFlexTable.getRowCount();
            costumersFlexTable.setText(row, 0, costumer.getCostumerTitle().toString().toLowerCase());
            costumersFlexTable.setText(row, 1, costumer.getFirstName());
            costumersFlexTable.setText(row, 2, costumer.getLastName());
            costumersFlexTable.setText(row, 3, costumer.getType().getCostumerType().toLowerCase());
            costumersFlexTable.setWidget(row, 4, removeCostumerButton);
            costumersFlexTable.setWidget(row, 5, editCostumerButton);
        }
    }
}