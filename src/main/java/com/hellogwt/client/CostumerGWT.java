package com.hellogwt.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;

public class CostumerGWT implements EntryPoint {

    @Override
    public void onModuleLoad() {
        CostumerGWTWidget costumerGWTWidget = GWT.create(CostumerGWTWidget.class);

        RootPanel.get().add(costumerGWTWidget);
    }
}