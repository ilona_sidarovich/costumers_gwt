package com.hellogwt.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.hellogwt.shared.domain.Type;
import com.hellogwt.shared.exception.ServiceException;

/**
 * Created by ilona on 10.09.15.
 */
@RemoteServiceRelativePath("springGwtServices/typeService")
public interface TypeService extends RemoteService {
    public void createType(Type type) throws ServiceException;
}
