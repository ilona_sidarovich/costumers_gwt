package com.hellogwt.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.hellogwt.shared.domain.Type;
import com.hellogwt.shared.exception.ServiceException;

/**
 * Created by ilona on 10.09.15.
 */
public interface TypeServiceAsync {
    public void createType(Type type, AsyncCallback<Void> async) throws ServiceException;
}
