package com.hellogwt.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.hellogwt.shared.exception.ServiceException;
import com.hellogwt.shared.domain.Costumer;

import java.util.List;


public interface CostumerServiceAsync {
    public void createCostumer(Costumer costumer, AsyncCallback<Void> async) throws ServiceException;
    public void viewAllCostumers(AsyncCallback<List<Costumer>> async) throws ServiceException;
    public void deleteCostumer(Long costumer, AsyncCallback<Void> async) throws ServiceException;
    public void getCostumerById(Long id, AsyncCallback<Costumer> async) throws ServiceException;
    public void updateCostumer(Costumer costumer, AsyncCallback<Void> async) throws ServiceException;
    public void findCostumerByFirstLastNameMethaphone(String firstName, String lastName, AsyncCallback<List> async) throws ServiceException;
}