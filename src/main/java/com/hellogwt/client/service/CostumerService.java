package com.hellogwt.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.hellogwt.shared.exception.ServiceException;
import com.hellogwt.shared.domain.Costumer;


import java.util.List;

@RemoteServiceRelativePath("springGwtServices/costumerService")
public interface CostumerService extends RemoteService {

    public void createCostumer(Costumer costumer) throws ServiceException;
    public List<Costumer> viewAllCostumers() throws ServiceException;
    public void deleteCostumer(Long costumer) throws ServiceException;
    public Costumer getCostumerById(Long id) throws ServiceException;
    public void updateCostumer(Costumer costumer) throws ServiceException;
    public List<Costumer> findCostumerByFirstLastNameMethaphone(String firstName, String lastName) throws ServiceException;

}