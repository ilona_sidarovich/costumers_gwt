package com.hellogwt.shared.domain.enums;

/**
 * Created by ilona on 01.09.15.
 */

public enum CostumerType {
    RESIDENTIAL, SMALL_MEDIUM_BUSINESS, ENTERPRISE
}
