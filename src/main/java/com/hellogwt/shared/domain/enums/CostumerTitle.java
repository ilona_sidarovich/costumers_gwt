package com.hellogwt.shared.domain.enums;

/**
 * Created by ilona on 28.08.15.
 */
public enum CostumerTitle {
    MRS, MR, DR, MS
}
