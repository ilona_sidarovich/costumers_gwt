package com.hellogwt.shared.domain;




import org.springframework.stereotype.Component;
import com.hellogwt.shared.domain.enums.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by ilona on 27.08.15.
 */
@Component
@Entity
@Table(name = ("COSTUMERS"))
public class Costumer implements Serializable{
    @Id
    @Column(name = ("COSTUMER_ID"))
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    @Column(name = ("TITLE"))
    @Enumerated(EnumType.STRING)
    private CostumerTitle costumerTitle;
    @Column(name = ("FIRST_NAME"))
    private String firstName;
    @Column(name = ("FIRST_NAME_METAPHONE"))
    private String firstNameMetaphone;
    @Column(name = ("LAST_NAME"))
    private String lastName;
    @Column(name = ("LAST_NAME_METAPHONE"))
    private String lastNameMetaphone;
    @Column(name = ("MODIFIED_WHEN"))
    private Date modifiedWhen;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name=("TYPE_ID"))
    private Type type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CostumerTitle getCostumerTitle() {
        return costumerTitle;
    }

    public void setCostumerTitle(CostumerTitle costumerTitle) {
        this.costumerTitle = costumerTitle;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getModifiedWhen() {
        return modifiedWhen;
    }

    public void setModifiedWhen(Date modifiedWhen) {
        this.modifiedWhen = modifiedWhen;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getFirstNameMetaphone() {
        return firstNameMetaphone;
    }

    public void setFirstNameMetaphone(String firstNameMetaphone) {
        this.firstNameMetaphone = firstNameMetaphone;
    }

    public String getLastNameMetaphone() {
        return lastNameMetaphone;
    }

    public void setLastNameMetaphone(String lastNameMetaphone) {
        this.lastNameMetaphone = lastNameMetaphone;
    }
}
