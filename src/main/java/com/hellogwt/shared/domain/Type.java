package com.hellogwt.shared.domain;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by ilona on 01.09.15.
 */
@Component
@Entity
@Table(name = ("TYPE"))
public class Type implements Serializable {
    @Id
    @Column(name = ("TYPE_ID"))
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    @Column(name = ("COSTUMER_TYPE"))
    private String costumerType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCostumerType() {
        return costumerType;
    }

    public void setCostumerType(String costumerType) {
        this.costumerType = costumerType;
    }
}
