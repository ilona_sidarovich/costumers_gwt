package com.hellogwt.server.service;

import com.hellogwt.shared.exception.DAOException;
import com.hellogwt.server.dao.impl.CostumerDaoImpl;

import com.hellogwt.shared.exception.ServiceException;
import com.hellogwt.shared.domain.Costumer;
import org.apache.commons.codec.language.Metaphone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CostumerService implements com.hellogwt.client.service.CostumerService {
    @Autowired
    CostumerDaoImpl costumerDao;

    public void createCostumer(Costumer costumer) throws ServiceException {
        costumer.setFirstNameMetaphone(new Metaphone().metaphone(costumer.getFirstName()));
        costumer.setLastNameMetaphone(new Metaphone().metaphone(costumer.getLastName()));
        try {
            costumerDao.createCostumer(costumer);
        } catch (DAOException e) {
            throw new ServiceException();
        }
    }

    public List<Costumer> viewAllCostumers() throws ServiceException {
        List<Costumer> costumers = new ArrayList<Costumer>();
        try {
            costumers = costumerDao.findAll();
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return costumers;
    }
    public void deleteCostumer(Long costumer) throws ServiceException {
        try {
            costumerDao.deleteCostumer(costumer);
        } catch (DAOException e) {
            throw new ServiceException();
        }
    }

    public Costumer getCostumerById(Long id) throws ServiceException{
        Costumer costumer = new Costumer();
        try {
            costumer=costumerDao.findCostumerById(id);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return costumer;
    }

    public void updateCostumer (Costumer costumer) throws ServiceException {
        try {
            costumerDao.update(costumer);
        } catch (DAOException e) {
            throw new ServiceException();
        }
    }

    public List<Costumer> findCostumerByFirstLastNameMethaphone(String firstName, String lastName) throws ServiceException{
        List<Costumer> costumers;
        try {
            costumers=costumerDao.findCostumerByFirstLastNameMethaphone(firstName, lastName);
        } catch (DAOException e) {
            throw new ServiceException();
        }
        return costumers;
    }
}