package com.hellogwt.server.service;

import com.hellogwt.shared.exception.DAOException;
import com.hellogwt.server.dao.impl.TypeDaoImpl;

import com.hellogwt.shared.exception.ServiceException;
import com.hellogwt.shared.domain.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TypeService implements com.hellogwt.client.service.TypeService{
    @Autowired
    TypeDaoImpl typeDao;
    public void createType(Type type) throws ServiceException {
        try {
            typeDao.createType(type);
        } catch (DAOException e) {
            throw new ServiceException();
        }
    }
}