
package com.hellogwt.server.dao.impl;

import com.hellogwt.shared.exception.DAOException;
import com.hellogwt.server.dao.TypeDao;
import com.hellogwt.shared.domain.Type;
import com.hellogwt.server.util.HibernateUtil;
//import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

@Repository
public class TypeDaoImpl implements TypeDao {
   // private static final Logger LOG = Logger.getLogger(TypeDaoImpl.class);
    @Override
    public void createType(Type type) throws DAOException {
        Session session = HibernateUtil.createSessionFactory().getCurrentSession();
        try {
            session.beginTransaction();
            session.save(type);
            session.getTransaction().commit();

        } catch (HibernateException e) {
          //  LOG.error(e);
            session.getTransaction().rollback();
        }
    }
}
