package com.hellogwt.server.dao.impl;

import com.hellogwt.shared.exception.DAOException;
import com.hellogwt.server.dao.CostumerDao;
import com.hellogwt.server.util.HibernateUtil;
import com.hellogwt.shared.domain.Costumer;
import org.apache.commons.codec.language.Metaphone;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class CostumerDaoImpl implements CostumerDao {
   //private static final Logger LOG = Logger.getLogger(CostumerDaoImpl.class);

    @Override
    public void createCostumer(Costumer costumer) throws DAOException {
        Session session = HibernateUtil.createSessionFactory().getCurrentSession();
        costumer.setModifiedWhen(new Date());
        try {
            session.beginTransaction();
            session.save(costumer);
            session.getTransaction().commit();
           // LOG.info(hellogwt.getFirstName() + " " + hellogwt.getLastName()+ " has been created");
        } catch (HibernateException e) {
           // LOG.error(e);

            session.getTransaction().rollback();
        }
    }

    @Override
    public List<Costumer> findAll() throws DAOException {
        Session session = HibernateUtil.createSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<Costumer> costumers = session.createCriteria(Costumer.class)
                .addOrder(Order.desc("modifiedWhen"))
                .setMaxResults(10)
                .list();
        session.getTransaction().commit();
        return costumers;
    }

    @Override
    public void update(Costumer costumer) throws DAOException {

        //LOG.debug("Editing existing person");

        Session session = HibernateUtil.createSessionFactory().getCurrentSession();
        session.beginTransaction();
        Costumer existingCostumer = (Costumer) session.get(Costumer.class, costumer.getId());
        existingCostumer.setFirstName(costumer.getFirstName());
        existingCostumer.setLastName(costumer.getLastName());
        existingCostumer.setType(existingCostumer.getType());
        existingCostumer.setModifiedWhen(new Date());
        try {
            session.save(existingCostumer);
            session.getTransaction().commit();
          //  LOG.info(hellogwt.getFirstName() + " " + hellogwt.getLastName()+ " has been updated");

        } catch (HibernateException e) {
           // LOG.error(e);
            session.getTransaction().rollback();
        }
    }

    @Override
    public void deleteCostumer(Long id) throws DAOException {
        Session session = HibernateUtil.createSessionFactory().getCurrentSession();
        try {
            session.beginTransaction();
            Costumer costumer = (Costumer) session.get(Costumer.class, id);
            session.delete(costumer);
            session.getTransaction().commit();
        }
        catch (HibernateException e) {
           // LOG.error(e);
            session.getTransaction().rollback();
        }
    }

    @Override
    public Costumer findCostumerById(Long id) throws DAOException {
        Session session = HibernateUtil.createSessionFactory().getCurrentSession();
        session.beginTransaction();
        Costumer costumer = (Costumer) session.get(Costumer.class, id);
        session.getTransaction().commit();
        return costumer;
    }

    public List<Costumer> findCostumerByFirstLastNameMethaphone(String firstName, String lastName) throws DAOException{
        Session session = HibernateUtil.createSessionFactory().getCurrentSession();
        session.beginTransaction();
        System.out.println(new Metaphone().metaphone(firstName));
        System.out.println(new Metaphone().metaphone(lastName));
        List<Costumer> costumers =
                session.createCriteria(Costumer.class)
                        .add(Restrictions.like("firstNameMetaphone", new Metaphone().metaphone(firstName)))
                        .add(Restrictions.like("lastNameMetaphone", new Metaphone().metaphone(lastName)))
                        .list();
        session.getTransaction().commit();
        return costumers;
    }
}
