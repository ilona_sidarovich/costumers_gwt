package com.hellogwt.server.dao;

import com.hellogwt.shared.exception.DAOException;
import com.hellogwt.shared.domain.Type;


public interface TypeDao {
    public void createType(Type type) throws DAOException;
}
