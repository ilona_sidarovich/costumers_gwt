package com.hellogwt.server.dao;

import com.hellogwt.shared.exception.DAOException;
import com.hellogwt.shared.domain.Costumer;

import java.util.List;

public interface CostumerDao {
    public void createCostumer(Costumer costumer) throws DAOException;
    public List<Costumer> findAll() throws DAOException;
    public void update(Costumer costumer) throws DAOException;
    public void deleteCostumer(Long id) throws DAOException;
    public Costumer findCostumerById(Long id) throws DAOException;
}
