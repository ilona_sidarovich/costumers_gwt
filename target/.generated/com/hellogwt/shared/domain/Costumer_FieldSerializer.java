package com.hellogwt.shared.domain;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Costumer_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native com.hellogwt.shared.domain.enums.CostumerTitle getCostumerTitle(com.hellogwt.shared.domain.Costumer instance) /*-{
    return instance.@com.hellogwt.shared.domain.Costumer::costumerTitle;
  }-*/;
  
  private static native void setCostumerTitle(com.hellogwt.shared.domain.Costumer instance, com.hellogwt.shared.domain.enums.CostumerTitle value) 
  /*-{
    instance.@com.hellogwt.shared.domain.Costumer::costumerTitle = value;
  }-*/;
  
  private static native java.lang.String getFirstName(com.hellogwt.shared.domain.Costumer instance) /*-{
    return instance.@com.hellogwt.shared.domain.Costumer::firstName;
  }-*/;
  
  private static native void setFirstName(com.hellogwt.shared.domain.Costumer instance, java.lang.String value) 
  /*-{
    instance.@com.hellogwt.shared.domain.Costumer::firstName = value;
  }-*/;
  
  private static native java.lang.String getFirstNameMetaphone(com.hellogwt.shared.domain.Costumer instance) /*-{
    return instance.@com.hellogwt.shared.domain.Costumer::firstNameMetaphone;
  }-*/;
  
  private static native void setFirstNameMetaphone(com.hellogwt.shared.domain.Costumer instance, java.lang.String value) 
  /*-{
    instance.@com.hellogwt.shared.domain.Costumer::firstNameMetaphone = value;
  }-*/;
  
  private static native java.lang.Long getId(com.hellogwt.shared.domain.Costumer instance) /*-{
    return instance.@com.hellogwt.shared.domain.Costumer::id;
  }-*/;
  
  private static native void setId(com.hellogwt.shared.domain.Costumer instance, java.lang.Long value) 
  /*-{
    instance.@com.hellogwt.shared.domain.Costumer::id = value;
  }-*/;
  
  private static native java.lang.String getLastName(com.hellogwt.shared.domain.Costumer instance) /*-{
    return instance.@com.hellogwt.shared.domain.Costumer::lastName;
  }-*/;
  
  private static native void setLastName(com.hellogwt.shared.domain.Costumer instance, java.lang.String value) 
  /*-{
    instance.@com.hellogwt.shared.domain.Costumer::lastName = value;
  }-*/;
  
  private static native java.lang.String getLastNameMetaphone(com.hellogwt.shared.domain.Costumer instance) /*-{
    return instance.@com.hellogwt.shared.domain.Costumer::lastNameMetaphone;
  }-*/;
  
  private static native void setLastNameMetaphone(com.hellogwt.shared.domain.Costumer instance, java.lang.String value) 
  /*-{
    instance.@com.hellogwt.shared.domain.Costumer::lastNameMetaphone = value;
  }-*/;
  
  private static native java.util.Date getModifiedWhen(com.hellogwt.shared.domain.Costumer instance) /*-{
    return instance.@com.hellogwt.shared.domain.Costumer::modifiedWhen;
  }-*/;
  
  private static native void setModifiedWhen(com.hellogwt.shared.domain.Costumer instance, java.util.Date value) 
  /*-{
    instance.@com.hellogwt.shared.domain.Costumer::modifiedWhen = value;
  }-*/;
  
  private static native com.hellogwt.shared.domain.Type getType(com.hellogwt.shared.domain.Costumer instance) /*-{
    return instance.@com.hellogwt.shared.domain.Costumer::type;
  }-*/;
  
  private static native void setType(com.hellogwt.shared.domain.Costumer instance, com.hellogwt.shared.domain.Type value) 
  /*-{
    instance.@com.hellogwt.shared.domain.Costumer::type = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.hellogwt.shared.domain.Costumer instance) throws SerializationException {
    com.google.gwt.core.client.impl.WeakMapping.set(instance, "server-enhanced-data-1", streamReader.readString());
    setCostumerTitle(instance, (com.hellogwt.shared.domain.enums.CostumerTitle) streamReader.readObject());
    setFirstName(instance, streamReader.readString());
    setFirstNameMetaphone(instance, streamReader.readString());
    setId(instance, (java.lang.Long) streamReader.readObject());
    setLastName(instance, streamReader.readString());
    setLastNameMetaphone(instance, streamReader.readString());
    setModifiedWhen(instance, (java.util.Date) streamReader.readObject());
    setType(instance, (com.hellogwt.shared.domain.Type) streamReader.readObject());
    
  }
  
  public static com.hellogwt.shared.domain.Costumer instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.hellogwt.shared.domain.Costumer();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.hellogwt.shared.domain.Costumer instance) throws SerializationException {
    streamWriter.writeString((String) com.google.gwt.core.client.impl.WeakMapping.get(instance, "server-enhanced-data-1"));
    streamWriter.writeObject(getCostumerTitle(instance));
    streamWriter.writeString(getFirstName(instance));
    streamWriter.writeString(getFirstNameMetaphone(instance));
    streamWriter.writeObject(getId(instance));
    streamWriter.writeString(getLastName(instance));
    streamWriter.writeString(getLastNameMetaphone(instance));
    streamWriter.writeObject(getModifiedWhen(instance));
    streamWriter.writeObject(getType(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.hellogwt.shared.domain.Costumer_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.hellogwt.shared.domain.Costumer_FieldSerializer.deserialize(reader, (com.hellogwt.shared.domain.Costumer)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.hellogwt.shared.domain.Costumer_FieldSerializer.serialize(writer, (com.hellogwt.shared.domain.Costumer)object);
  }
  
}
