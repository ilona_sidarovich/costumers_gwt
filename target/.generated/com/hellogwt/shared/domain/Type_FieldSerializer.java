package com.hellogwt.shared.domain;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Type_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getCostumerType(com.hellogwt.shared.domain.Type instance) /*-{
    return instance.@com.hellogwt.shared.domain.Type::costumerType;
  }-*/;
  
  private static native void setCostumerType(com.hellogwt.shared.domain.Type instance, java.lang.String value) 
  /*-{
    instance.@com.hellogwt.shared.domain.Type::costumerType = value;
  }-*/;
  
  private static native java.lang.Long getId(com.hellogwt.shared.domain.Type instance) /*-{
    return instance.@com.hellogwt.shared.domain.Type::id;
  }-*/;
  
  private static native void setId(com.hellogwt.shared.domain.Type instance, java.lang.Long value) 
  /*-{
    instance.@com.hellogwt.shared.domain.Type::id = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.hellogwt.shared.domain.Type instance) throws SerializationException {
    com.google.gwt.core.client.impl.WeakMapping.set(instance, "server-enhanced-data-1", streamReader.readString());
    setCostumerType(instance, streamReader.readString());
    setId(instance, (java.lang.Long) streamReader.readObject());
    
  }
  
  public static com.hellogwt.shared.domain.Type instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.hellogwt.shared.domain.Type();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.hellogwt.shared.domain.Type instance) throws SerializationException {
    streamWriter.writeString((String) com.google.gwt.core.client.impl.WeakMapping.get(instance, "server-enhanced-data-1"));
    streamWriter.writeString(getCostumerType(instance));
    streamWriter.writeObject(getId(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.hellogwt.shared.domain.Type_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.hellogwt.shared.domain.Type_FieldSerializer.deserialize(reader, (com.hellogwt.shared.domain.Type)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.hellogwt.shared.domain.Type_FieldSerializer.serialize(writer, (com.hellogwt.shared.domain.Type)object);
  }
  
}
