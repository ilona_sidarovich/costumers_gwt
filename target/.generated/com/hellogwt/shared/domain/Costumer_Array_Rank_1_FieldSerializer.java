package com.hellogwt.shared.domain;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Costumer_Array_Rank_1_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, com.hellogwt.shared.domain.Costumer[] instance) throws SerializationException {
    com.google.gwt.user.client.rpc.core.java.lang.Object_Array_CustomFieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.hellogwt.shared.domain.Costumer[] instantiate(SerializationStreamReader streamReader) throws SerializationException {
    int size = streamReader.readInt();
    return new com.hellogwt.shared.domain.Costumer[size];
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.hellogwt.shared.domain.Costumer[] instance) throws SerializationException {
    com.google.gwt.user.client.rpc.core.java.lang.Object_Array_CustomFieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.hellogwt.shared.domain.Costumer_Array_Rank_1_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.hellogwt.shared.domain.Costumer_Array_Rank_1_FieldSerializer.deserialize(reader, (com.hellogwt.shared.domain.Costumer[])object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.hellogwt.shared.domain.Costumer_Array_Rank_1_FieldSerializer.serialize(writer, (com.hellogwt.shared.domain.Costumer[])object);
  }
  
}
