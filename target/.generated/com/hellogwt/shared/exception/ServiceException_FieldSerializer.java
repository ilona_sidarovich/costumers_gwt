package com.hellogwt.shared.exception;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class ServiceException_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, com.hellogwt.shared.exception.ServiceException instance) throws SerializationException {
    
    com.google.gwt.user.client.rpc.core.java.lang.Exception_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.hellogwt.shared.exception.ServiceException instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.hellogwt.shared.exception.ServiceException();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.hellogwt.shared.exception.ServiceException instance) throws SerializationException {
    
    com.google.gwt.user.client.rpc.core.java.lang.Exception_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.hellogwt.shared.exception.ServiceException_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.hellogwt.shared.exception.ServiceException_FieldSerializer.deserialize(reader, (com.hellogwt.shared.exception.ServiceException)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.hellogwt.shared.exception.ServiceException_FieldSerializer.serialize(writer, (com.hellogwt.shared.exception.ServiceException)object);
  }
  
}
