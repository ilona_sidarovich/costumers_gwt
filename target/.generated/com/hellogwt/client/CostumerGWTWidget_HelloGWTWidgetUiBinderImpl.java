package com.hellogwt.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiBinderUtil;
import com.google.gwt.user.client.ui.Widget;

public class CostumerGWTWidget_HelloGWTWidgetUiBinderImpl implements UiBinder<com.google.gwt.user.client.ui.Widget, com.hellogwt.client.CostumerGWTWidget>, com.hellogwt.client.CostumerGWTWidget.HelloGWTWidgetUiBinder {

  public com.google.gwt.user.client.ui.Widget createAndBindUi(final com.hellogwt.client.CostumerGWTWidget owner) {

    com.hellogwt.client.CostumerGWTWidget_HelloGWTWidgetUiBinderImpl_GenBundle clientBundleFieldNameUnlikelyToCollideWithUserSpecifiedFieldOkay = (com.hellogwt.client.CostumerGWTWidget_HelloGWTWidgetUiBinderImpl_GenBundle) GWT.create(com.hellogwt.client.CostumerGWTWidget_HelloGWTWidgetUiBinderImpl_GenBundle.class);
    com.google.gwt.user.client.ui.Label f_Label3 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
    com.google.gwt.user.client.ui.ListBox titleBox = (com.google.gwt.user.client.ui.ListBox) GWT.create(com.google.gwt.user.client.ui.ListBox.class);
    com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel2 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
    com.google.gwt.user.client.ui.Label f_Label5 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
    com.google.gwt.user.client.ui.TextBox firstNameTextBox = (com.google.gwt.user.client.ui.TextBox) GWT.create(com.google.gwt.user.client.ui.TextBox.class);
    com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel4 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
    com.google.gwt.user.client.ui.Label f_Label7 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
    com.google.gwt.user.client.ui.TextBox lastNameTextBox = (com.google.gwt.user.client.ui.TextBox) GWT.create(com.google.gwt.user.client.ui.TextBox.class);
    com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel6 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
    com.google.gwt.user.client.ui.Label f_Label9 = (com.google.gwt.user.client.ui.Label) GWT.create(com.google.gwt.user.client.ui.Label.class);
    com.google.gwt.user.client.ui.ListBox typeBox = (com.google.gwt.user.client.ui.ListBox) GWT.create(com.google.gwt.user.client.ui.ListBox.class);
    com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel8 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
    com.google.gwt.user.client.ui.Button addButton = (com.google.gwt.user.client.ui.Button) GWT.create(com.google.gwt.user.client.ui.Button.class);
    com.google.gwt.user.client.ui.HorizontalPanel f_HorizontalPanel10 = (com.google.gwt.user.client.ui.HorizontalPanel) GWT.create(com.google.gwt.user.client.ui.HorizontalPanel.class);
    com.google.gwt.user.client.ui.FlexTable costumersFlexTable = (com.google.gwt.user.client.ui.FlexTable) GWT.create(com.google.gwt.user.client.ui.FlexTable.class);
    com.google.gwt.user.client.ui.VerticalPanel f_VerticalPanel1 = (com.google.gwt.user.client.ui.VerticalPanel) GWT.create(com.google.gwt.user.client.ui.VerticalPanel.class);

    f_Label3.setWidth("50");
    f_Label3.setText("Title:");
    f_HorizontalPanel2.add(f_Label3);
    titleBox.addItem("Mr", "MR");
    titleBox.addItem("Ms", "MS");
    titleBox.addItem("Mrs", "MRS");
    titleBox.addItem("Dr", "DR");
    titleBox.setWidth("100");
    f_HorizontalPanel2.add(titleBox);
    f_HorizontalPanel2.setHeight("20");
    f_VerticalPanel1.add(f_HorizontalPanel2);
    f_Label5.setWidth("50");
    f_Label5.setText("First Name:");
    f_HorizontalPanel4.add(f_Label5);
    firstNameTextBox.setWidth("100");
    f_HorizontalPanel4.add(firstNameTextBox);
    f_HorizontalPanel4.setHeight("20");
    f_VerticalPanel1.add(f_HorizontalPanel4);
    f_Label7.setWidth("50");
    f_Label7.setText("Last Name:");
    f_HorizontalPanel6.add(f_Label7);
    lastNameTextBox.setWidth("100");
    f_HorizontalPanel6.add(lastNameTextBox);
    f_HorizontalPanel6.setHeight("20");
    f_VerticalPanel1.add(f_HorizontalPanel6);
    f_Label9.setWidth("50");
    f_Label9.setText("Type:");
    f_HorizontalPanel8.add(f_Label9);
    typeBox.addItem("Residental", "RESIDENTAL");
    typeBox.addItem("Small/Medium Business", "SMALL_MEDIUM_BUSINESS");
    typeBox.addItem("Enterprise", "ENTERPRISE");
    typeBox.setWidth("100");
    f_HorizontalPanel8.add(typeBox);
    f_HorizontalPanel8.setHeight("20");
    f_VerticalPanel1.add(f_HorizontalPanel8);
    addButton.setWidth("50");
    addButton.setText("Add");
    f_HorizontalPanel10.add(addButton);
    f_HorizontalPanel10.setHeight("20");
    f_VerticalPanel1.add(f_HorizontalPanel10);
    f_VerticalPanel1.add(costumersFlexTable);



    final com.google.gwt.event.dom.client.ClickHandler handlerMethodWithNameVeryUnlikelyToCollideWithUserFieldNames1 = new com.google.gwt.event.dom.client.ClickHandler() {
      public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
        owner.handleAddButtonClick(event);
      }
    };
    addButton.addClickHandler(handlerMethodWithNameVeryUnlikelyToCollideWithUserFieldNames1);

    owner.addButton = addButton;
    owner.costumersFlexTable = costumersFlexTable;
    owner.firstNameTextBox = firstNameTextBox;
    owner.lastNameTextBox = lastNameTextBox;
    owner.titleBox = titleBox;
    owner.typeBox = typeBox;

    return f_VerticalPanel1;
  }
}
