package com.hellogwt.client.service;

import com.google.gwt.user.client.rpc.impl.RemoteServiceProxy;
import com.google.gwt.user.client.rpc.impl.ClientSerializationStreamWriter;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.impl.RequestCallbackAdapter.ResponseReader;
import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.RpcToken;
import com.google.gwt.user.client.rpc.RpcTokenException;
import com.google.gwt.core.client.impl.Impl;
import com.google.gwt.user.client.rpc.impl.RpcStatsContext;

public class TypeService_Proxy extends RemoteServiceProxy implements com.hellogwt.client.service.TypeServiceAsync {
  private static final String REMOTE_SERVICE_INTERFACE_NAME = "com.hellogwt.client.service.TypeService";
  private static final String SERIALIZATION_POLICY ="40FB6014247F4E4F860B3E9E427A6719";
  private static final com.hellogwt.client.service.TypeService_TypeSerializer SERIALIZER = new com.hellogwt.client.service.TypeService_TypeSerializer();
  
  public TypeService_Proxy() {
    super(GWT.getModuleBaseURL(),
      "springGwtServices/typeService", 
      SERIALIZATION_POLICY, 
      SERIALIZER);
  }
  
  public void createType(com.hellogwt.shared.domain.Type type, com.google.gwt.user.client.rpc.AsyncCallback async) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("TypeService_Proxy", "createType");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 1);
      streamWriter.writeString("com.hellogwt.shared.domain.Type/574686537");
      streamWriter.writeObject(type);
      helper.finish(async, ResponseReader.VOID);
    } catch (SerializationException ex) {
      async.onFailure(ex);
    }
  }
  @Override
  public SerializationStreamWriter createStreamWriter() {
    ClientSerializationStreamWriter toReturn =
      (ClientSerializationStreamWriter) super.createStreamWriter();
    if (getRpcToken() != null) {
      toReturn.addFlags(ClientSerializationStreamWriter.FLAG_RPC_TOKEN_INCLUDED);
    }
    return toReturn;
  }
  @Override
  protected void checkRpcTokenType(RpcToken token) {
    if (!(token instanceof com.google.gwt.user.client.rpc.XsrfToken)) {
      throw new RpcTokenException("Invalid RpcToken type: expected 'com.google.gwt.user.client.rpc.XsrfToken' but got '" + token.getClass() + "'");
    }
  }
}
